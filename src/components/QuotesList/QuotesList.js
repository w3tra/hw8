import React, {Component} from 'react';
import axios from 'axios';
import Aside from "../Aside/Aside";
import {Link} from "react-router-dom";

class QuotesList extends Component {

  state = {
    quotesList: [],
    urlId: ''
  };

  getQuote = () =>{
    let id = this.props.match.params.id;
    if (id === 'all') {
      axios.get('/quotes.json').then(r => {
        this.setState({quotesList: r.data, urlId: this.props.match.params.id});
      });
    } else {
      axios.get(`/quotes.json?orderBy="category"&equalTo="${id}"`).then(response => {
        this.setState({quotesList: response.data, urlId: this.props.match.params.id});
      });
    }
  };

  deleteQuoteHandler = (id) => {
    axios.delete(`/quotes/${id}.json`).then(() => {
      this.getQuote();
    });
  };

  componentDidMount() {
    this.getQuote();
  }

  componentDidUpdate() {
    if (this.props.match.params.id !== this.state.urlId) {
      this.getQuote();
    }
  }

  render() {
    if (this.state.quotesList) {
      return(
        <div className="quotes-wrapper">
          <Aside/>
          <div className="quotes-list">
            {Object.keys(this.state.quotesList).map(itemId => {
              return (
                <div key={itemId} className="quotesList-item">
                  <div className="quote-item">
                    <blockquote>
                      <p>{this.state.quotesList[itemId].text}</p>
                      <footer>— <cite>{this.state.quotesList[itemId].author}</cite></footer>
                    </blockquote>
                  </div>
                  <div className="icons-panel">
                    <i className="material-icons icon icon-delete" onClick={() => this.deleteQuoteHandler(itemId)}>delete_sweep</i>
                    <Link to={`/quotes/${itemId}/edit-quote`}><i className="material-icons icon icon-edit">create</i></Link>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      )
    }
  }
}

export default QuotesList;