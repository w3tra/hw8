import React, { Component } from 'react';
import axios from 'axios';
import './AddQuote.css';
import ViewAddForm from "../../components/ViewAddForm/ViewAddForm";

class AddQuote extends Component {

  state = {
    category: '',
    author: '',
    text: ''
  };

  changeValueHandler = (event) => {
    this.setState({[event.target.name]: event.target.value})
  };

  changeSelectValue = (e) => {
    this.setState({category: e.target.value});
  };

  savePostHandler = () => {
    const quote = {category: this.state.category, author: this.state.author, text: this.state.text};
    axios.post('/quotes.json', quote).then(() => {
      this.props.history.push('/quotes');
    })
  };

  render() {
    return(
      <ViewAddForm
        changedSelect={(event) => this.changeSelectValue(event)}
        changedInput={(event) => this.changeValueHandler(event)}
        changedText={(event) => this.changeValueHandler(event)}
        saved={this.savePostHandler}
      />
    )
  }
}

export default AddQuote;